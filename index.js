const express = require('express');
require('dotenv').config();
const multer = require('multer');
const multerS3 = require('multer-s3');
const AWS = require('aws-sdk');
const { v4 }   = require('uuid');
const cors = require('cors');

const port = process.env.PORT || 8303;

AWS.config.update({
    accessKeyId:process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey:process.env.AWS_SECRET_ACCESS_KEY
});
const myBucket = process.env.AWS_BUCKET_NAME;

const app = express();

const s3 = new AWS.S3();

const upload = multer({
    storage: multerS3({
        s3:s3,
        bucket:myBucket,
        // acl:"public-read",
        contentType: multerS3.AUTO_CONTENT_TYPE,
        key:function(req,file,ab_callback){
            //file.originalname
            const newFileName = Date.now() + "-" + v4() +"."+ file.originalname.split('.')[1]
            const fullPath = `${req.params.prefix}/` + newFileName;
            ab_callback(null,fullPath);
        }
    })
});



app.use(cors({ origin: '*',}));
app.use(express.json());

app.get('/',(req,res,next) =>{
    res.render('index');
})

app.post('/uploads/delete',async(req, res) => {
    console.log(req.body);
    const {keyFile} = req.body;
    s3.deleteObject({ Bucket: myBucket, Key: keyFile }, (err, data) => {
        console.error(err);
        console.log(data);
        if(data){
            return res.status(200).json({
                status:1,
                message:"Deleted successfully",
                data:data
            })
        }
        if(err){
            return res.status(401).json({
                status:0,
                message:"Error! delete failed",
                data:err
            })
        }
    });
    
    
});
app.post('/uploads/:prefix',upload.single('myfile'),(req,res) =>{
    console.log(req.file);
    if(req.file){
        return res.status(200).json({
            status:1,
            message:"success! Upload",
            data:req.file
        })
    }
    return res.status(401).json({
        status:0,
        message:"Error! Upload",
        data:req.file
    })
})

// (async () =>{
//     await s3.putObject({
//         Body: "hello world",
//         Bucket:"4sharks-absat",
//         Key: "test.txt",
//     })
//     .promise();
// })();

app.listen(port,()=>{
    console.log(`listing on http://localhost:${port}`);
});